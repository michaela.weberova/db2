const express = require("express");

const recordRoutes = express.Router();

const dbo = require("../db/conn");

recordRoutes.route("/").get(async function (req, res) {
    res.render('../views/pages/about');
});

recordRoutes.route("/comments").get(async function (req, res) {
    const dbConnect = dbo.getDb();

    dbConnect
        .collection("comments")
        .find({})
        .limit(50)
        .toArray(function (err, result) {
        if(err) {
            res.status(400).send("Error fetching comments.");
        }
        else {
            res.render('../views/pages/index', {
                mascots: result
            });
        }});
});

recordRoutes.route("/comments/email").get(async function (req, res) {
    const dbConnect = dbo.getDb();
    const listingQuery = { email: req.body.email };

    dbConnect
        .collection("comments")
        .find(listingQuery)
        .limit(50)
        .toArray(function (err, result) {
            if(err) {
                res.status(400).send("Error fetching comments.");
            }
            else {
                res.json(result);
            }
        });
});

recordRoutes.route("/comments/create").post(function (req, res) {
    const dbConnect = dbo.getDb();
    const matchDocument = {
        name: req.body.name,
        email: req.body.email,
        movie_id: "ObjectId('573a1390f29313caabcd516c')",
        text: req.body.text,
        last_modified: new Date()
    };

    dbConnect
        .collection("comments")
        .insertOne(matchDocument, function (err, result) {
            if (err) {
                res.status(400).send("Error inserting comment!");
            } else {
                console.log(`Added a new comment with id ${result.insertedId}`);
                res.status(204).send();
            }
        });
});

recordRoutes.route("/comments/update").post(function (req, res) {
    const dbConnect = dbo.getDb();
    const listingQuery = { _id: req.body.id };
    const updates = {
        $set: {
            comment: req.body.comment
        },
    };

    dbConnect
        .collection("comments")
        .updateOne(listingQuery, updates, function (err, _result) {
            if (err) {
                res.status(400).send(`Error updating comment with id ${listingQuery.id}!`);
            } else {
                console.log("1 document updated");
            }
        });
});

recordRoutes.route("/comments/delete/:id").delete((req, res) => {
    const dbConnect = dbo.getDb();
    const commentQuery = { commentQuery: req.body.id };

    dbConnect
        .collection("comments")
        .deleteOne(commentQuery, function (err, _result) {
            if (err) {
                res.status(400).send(`Error deleting listing with id ${commentQuery.commentQuery}!`);
            } else {
                console.log("1 document deleted");
            }
        });
});

module.exports = recordRoutes;
